# Arduino CW keyboard

The project goal is to emulate Iambic morse keyers as a complete keyboard with Arduino.

# Installation

## Uploading the sketch

The first step is, like any other arduino programs, uploading the sketch to the *Arduino Uno* board, either using `arduino-cli` or the standard IDE:
```sh
git clone https://gitlab.com/f4iey/arduino-cw-keyboard.git
cd arduino-cw-keyboard
arduino arduino-cw-keyboard.ino
```
## Firmware mod

To make the Arduino Uno act as a keyboard controller, flashing the firmware is needed. To do so, `dfu-programmer` has to be installed on the system. It is available on most common distributions.

### Installing `dfu-programmer`

#### Debian based
Installation is made using `apt`
```sh
apt update
apt install dfu-programmer
```

#### Arch based
`dfu-programmer` is located in the *extra* repository in `pacman`:
```sh
pacman -Sy dfu-programmer
```

#### Mac OS
The program is available on both *Homebrew* and *MacPorts*
```sh
brew install dfu-programmer libusb-compat
```
```sh
port install dfu-programmer
```

### Flashing the firmware

This process is done by entreing into the atmega16u2 bootloader. In order to achieve this, simply short the nearest two pins from the USB port (the top pins of the AREF section). After a faction of time, the LED should blink.
![DFU Pins](https://i.stack.imgur.com/FHKp3.jpg)

You can check if you're indeed in the bootloarder by typing `lsusb | grep DFU`.

Once into the bootloader, flashing the firmware can be done using the simple script located in the repo:
```sh
cd fw
./set-keyboard.sh
```
Make sure to power-cycle the Arduino afterwards to apply changes.

To get back to normal mode (if you want to upload a new sketch), simply flash the firmware the same way but using the script `./set-serial.sh`

## Special thanks

Thanks to [Mattias Jähnke](https://github.com/mattiasjahnke) for the DFU scripts.
He also made a similar Arduino sketch for straight keys available [here](https://github.com/mattiasjahnke/arduino-projects/tree/master/morse-keyboard).
