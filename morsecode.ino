#include "morsecode.h"

/**
 * ARDUINO MORSECODE FUNCTIONS
 * here are the detailed functions
 * for the arduino morsecode library
 */

unsigned long wpmToMillis(int wpm) {
  //le mot de référence est PARIS: 50 éléments
  unsigned long ptMs = 60000/(wpm*50);
  return ptMs; 
}

void cwToString(String str){
  String morsecode[] = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", 
  "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...",
  "-", "..-", "...-", ".--", "-..-", "-.--", "--..", ".----", "..---", "...--",
  "....-", ".....", "-....", "--...", "---..", "----.", "-----", "--..--",
  "-...-", "-..-.", ".-.-.-", "..--.."};
  for(int i=0; i<41; i++){
    if(morsecode[i].equals(str)){
      //FR KB layout
      if(i==0) {sendChar(0x14); return;} //remplace q par a
      else if(i==16) {sendChar(0x04); return;} //remplace a par q
      else if(i==22) {sendChar(0x1D); return;} //remplace z par w
      else if(i==25) {sendChar(0x1A); return;} //remplace w par z
      else if(i==12) {sendChar(0x33); return;}//remplace , par m
      else if(i>=26 && i<36){
        //On press caps lock
        sendChar(0x39);
        sendChar(0x04 + i);
        sendChar(0x39); //on cancel caps lock
        return;
      }
      else if(i==36) {sendChar(0x10); return;} //ajoute la virgule
      else if(i==37) {sendChar(0x2E); return;} //ajoute =
      else if(i==38) {sendChar(0x39); sendChar(0x37); sendChar(0x39); return;} //ajoute la barre de fraction
      else if(i==39) {sendChar(0x39); sendChar(0x36); sendChar(0x39); return;} //ajoute le point
      else if(i==40) {sendChar(0x39); sendChar(0x10); sendChar(0x39); return;} //ajoute ?
      else { sendChar(0x04 + i); return;} //on envoie le caractère A + i
      //Serial.print((char)('A' + i));
    }
  }
}

void sendChar(uint8_t opcode){
  //pour envoyer des keystrokes à l'ordinateur
  uint8_t buff[8] = {0};
  buff[2] = opcode;
  Serial.write(buff, 8);
  //delay(200);
  buff[2] = 0;
  Serial.write(buff, 8);
  //delay(200);
}

void dot(){
  //on écrit un point
  paddle('.');
}

void dash(){https://gitlab.com/f4iey/arduino-chttps://gitlab.com/f4iey/arduino-cw-keyboard/-/raw/main/arduino-cw-keyboard.inow-keyboard/-/raw/main/arduino-cw-keyboard.ino
  //on écrit un trait
  paddle('-');
}

void paddle(char dd){
  //optimisation
    //tone(3, 600);
    digitalWrite(TX, HIGH);
    trx.output_enable(SI5351_CLK0, 1);
    trx.update_status();

    if(dd == '.') delay(tdot); else delay(tdash);
    //noTone(3);
    digitalWrite(TX, LOW);
    trx.output_enable(SI5351_CLK0, 0);
    trx.update_status();

    delay(tdot); //espace inter-charactère
    //str.concat(dd);
}

void sendMsg(String str)
{
  Serial.print("\nTX: ");
  for(int i=0;i<str.length();i++)
  {
    switch (str.charAt(i))
    {
    case 'A':
      dot();dash();break;
    case 'B':
      dash();dot();dot();dot();break;
    case 'C':
      dash();dot();dash();dot();break;
    case 'D':
      dash();dot();dot();break;
    case 'E':
      dot();break;
    case 'F':
      dot();dot();dash();dot();break;
    case 'G':
      dash();dash();dot();break;
    case 'H':
      dot();dot();dot();dot();break;
    case 'I':
      dot();dot();break;
    case 'J':
      dot();dash();dash();dash();break;
    case 'K':
      dash();dot();dash();break;
    case 'L':
      dot();dash();dot();dot();break;
    case 'M':
      dash();dash();break;
    case 'N':
      dash();dot();break;
    case 'O':
      dash();dash();dash();break;
    case 'P':
      dot();dash();dash();dot();break;
    case 'Q':
      dash();dash();dot();dash();break;
    case 'R':
      dot();dash();dot();break;
    case 'S':
      dot();dot();dot();break;
    case 'T':
      dash();break;
    case 'U':
      dot();dot();dash();break;
    case 'V':
      dot();dot();dot();dash();break;
    case 'W':
      dot();dash();dash();break;
    case 'X':
      dash();dot();dot();dash();break;
    case 'Y':
      dash();dot();dash();dash();break;
    case 'Z':
      dash();dash();dot();dot();break;
    case ' ':
      delay(tdot*4);
      break;
    case '.':
      dot();dash();dot();dash();dot();dash();break;
    case ',':
      dash();dash();dot();dot();dash();dash();break;
    case ':':
      dash();dash();dash();dot();dot();break;
    case '?':
      dot();dot();dash();dash();dot();dot();break;
    case '\'':
      dot();dash();dash();dash();dash();dot();break;
    case '-':
      dash();dot();dot();dot();dot();dash();break;
    case '/':
      dash();dot();dot();dash();dot();break;
    case '(':
    case ')':
      dash();dot();dash();dash();dot();dash();break;
    case '\"':
      dot();dash();dot();dot();dash();dot();break;
    case '@':
      dot();dash();dash();dot();dash();dot();break;
    case '=':
      dash();dot();dot();dot();dash();break;
    case '0':
     dash();dash();dash();dash();dash();break;
    case '1':
     dot();dash();dash();dash();dash();break;
    case '2':
     dot();dot();dash();dash();dash();break;
    case '3':
     dot();dot();dot();dash();dash();break;
    case '4':
     dot();dot();dot();dot();dash();break;
    case '5':
     dot();dot();dot();dot();dot();break;
    case '6':
     dash();dot();dot();dot();dot();break;
    case '7':
     dash();dash();dot();dot();dot();break;
    case '8':
     dash();dash();dash();dot();dot();break;
    case '9':
     dash();dash();dash();dash();dot();break;

    }
    Serial.print(str.charAt(i));
    delay(3*tdot); //espace entre chaque lettre
  }
}
