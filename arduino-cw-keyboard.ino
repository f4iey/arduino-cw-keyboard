/* SIMPLE MORSE ENCODER
 *  Ce programme écrit du texte manipulé en morse comme avec un clavier standard
 *  -----------------------------------------------------------------
 *  PINOUT:
 *  D3: Sortie audio (buzzer)
 *  D8: CH1 pour les points
 *  D9: CH2 pour les traits
 *  Les deux channels sont mis sur GND au point commum
 */
#include "morsecode.h"
#define CH1 8
#define CH2 9
#define TX 3
int state1 = LOW;
int state2 = LOW; 
int wpm = 15;
int potPin = A0; //ATTENTION: ENTRÉE STRICTEMENT ANL
int potVal = 0;
unsigned long tdot = 0;
unsigned long tdash = 0;
unsigned long idle = 69420;
String str = "";

void setup() {
  pinMode(potPin, INPUT);
  pinMode(CH1, INPUT);
  pinMode(CH2, INPUT);
  pinMode(3, OUTPUT);
  pinMode(TX, OUTPUT);
  digitalWrite(CH1, HIGH);
  digitalWrite(CH2, HIGH);
  Serial.begin(9600);
}

void loop() {
  potVal = analogRead(potPin); // variateur wpm
  state1 = digitalRead(CH1);
  state2 = digitalRead(CH2);
  wpm = map(potVal, 0, 1023, 10, 30);
  /* on fera varier la vitesse jusqu'a 50 wpm...
   *  ce qui est déjà bien rapide ^^
   */
  //mise en place des delays: 
  tdot = wpmToMillis(wpm);
  tdash = 3*tdot;
  //processus actif
  if(state1 == LOW) {
    dot();
    if(state2 == LOW) {
      dash();
    }
    idle = millis(); //on reset la durée idle
  }
  //on écrit l'inverse
  else if(state2 == LOW) {
    dash();
    if(state1 == LOW) {
      dot();
    }
    idle = millis(); //on reset la durée idle
  }
  //idle = millis() - idle; //durée pour laquelle le keyer est au repos
  if((millis() - idle) >= 2*tdot && (millis() - idle) < 3*tdot){
    //on passe au caractère ASCII suivant
    //Serial.println(str);
    cwToString(str);
    str = "";
  }
}
